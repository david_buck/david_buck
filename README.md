Gitlab only contains work (i.e. private contributions). Personal projects and contributions are to be found on my GitHub account.

  <img align="left" alt="David's GitHub Stats" src="https://github-readme-stats.vercel.app/api?username=davidxbuck&show_icons=true&hide_border=true" />
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=davidxbuck&layout=compact&theme=material-palenight" />

